function clickCreatePanel(elementName) {
    ////DEBUG
    console.log("CLICK CREATE PANEL -> begin");

    var panel = "";

    //stop any duplicate panels
    if (panel1 != elementName && panel2 != elementName && panel3 != elementName && panel4 != elementName) {
        //if panel 1 is up
        if (panel1 != "") {
            //if panel 2 is up
            if (panel2 != "") {
                //if panel 3 is up
                if (panel3 != "") {
                    //panel 4 not up so create
                    if (panel4 == "") {
                        panel4 = elementName;
                        createPanel(4, panel4);
                        panel = "panel4";
                    }
                } else {
                    //panel 3 not up so create
                    panel3 = elementName;
                    createPanel(3, panel3);
                    panel = "panel3";
                }
            } else {
                //panel 2 not up so create
                panel2 = elementName;
                createPanel(2, panel2);
                panel = "panel2";
                $("div.panel2 div.header").width($("div.panel2").width());
                if (panel3 != "") {
                    $("div.panel2").css({ "height": "60%" });
                }
            }
        } else {
            //panel 1 not up so create
            panel1 = elementName;
            createPanel(1, panel1);
            panel = "panel1";
            $("div.panel1 div.header").width($("div.panel1").width());
        }
    }

    if (elementName == "dropbox" && $("#dropboxContent .dropbox-item").length == 0 && $("#dropboxContent .dropbox-folder").length == 0) {
        GetDropBoxFiles('/');
    }

    if (elementName == "facebook") {
        GetFacebookPosts();
    }

    //DEBUG
    console.log("CLICK CREATE PANEL -> end");

    //return panel;
}

function createPanel(position, panelName) {
    console.log("CREATE PANEL -> begin");
    $("#footer ." + panelName).css({ "box-shadow": "0 0 7px rgb(40,170,225)" });
    $("#bodycontent").css("background", "rgb(252,252,252)");
    $("#bodycontent").append("<div class=\"panel" + position + "\"></div>");
    $("div.panel" + position).append("<div class=\"header\"><img class=\"header\" src=\"images/header-" + panelName + ".png\" /><img class=\"exit\" src=\"images/cross.png\" /></div>");
    $("div.panel" + position).toggleClass(panelName);    
    $("div.panel" + position).attr("id", panelName);

    if (panelName == "yahoo") { panelName = "yahooContentSmall"; } else { panelName = panelName + "Content"; }
    $("div.panel" + position).append($("#" + panelName));
    $("#" + panelName).css({ "display": "" });
    $("div.panel" + position + " img.exit").click(function () { removePanel("panel" + position); });
    $("div.panel" + position).droppable({
        drop: function (event, ui) { feedItemDragStop(event, $(this)); },
        greedy: true
    });
    setPanelDimensions(position);
    
    switch (position) {
        case 1:
            //setPanelDimensions(1);
            break;
        case 2:
            //setPanelDimensions(2);
            $("div.panel2").find("img.contentImage").width($("div.panel2 .post:first").width());
            $("div.panel2").find("iframe.contentVideo").width($("div.panel2 .post:first").width());
            break;
        case 3:
            //setPanelDimensions(3);           
            $("div.panel3").css("margin-top", "-3px");
            break;
        case 4:
            //setPanelDimensions(4);            
            $("div.panel4").css("margin-top", "-3px");
            break;
    }

    $("div.panel" + position).find("img.contentImage").width($("div.panel" + position + " .post:first").width());
    $("div.panel" + position).find("iframe.contentVideo").width($("div.panel" + position + " .post:first").width());
    console.log("CREATE PANEL -> end")
}

function removePanel(panel) {
    console.log("REMOVE PANEL -> begin");
    console.log("PANEL 1: " + panel1);
    console.log("PANEL 2: " + panel2);
    console.log("PANEL 3: " + panel3);
    console.log("PANEL 4: " + panel4);
    console.log(panel);
    switch (panel) {
        case "panel1":
            moveStaticContent(panel, panel1);
            $("div.panel1 img.exit").unbind("click");
            $("div.panel1").remove();
            panel1 = "";
            console.log("removed panel 1");

            if (panel2 != "") {
                panel1 = panel2;
                $("div.panel2 img.exit").unbind("click");
                $("div.panel2").removeClass("panel2 " + panel2).addClass("panel1 " + panel1);
                $("div.panel1 img.exit").click(function () { removePanel("panel1"); });
                setPanelDimensions(1);
                panel2 = "";
                console.log("moved panel 2");

                if (panel3 != "") {
                    panel2 = panel3;
                    $("div.panel3 img.exit").unbind("click");
                    $("div.panel3").removeClass("panel3 " + panel3).addClass("panel2 " + panel2);
                    $("div.panel2 img.exit").click(function () { removePanel("panel2"); });
                    setPanelDimensions(2);
                    panel3 = "";
                    console.log("moved panel 3");

                    if (panel4 != "") {
                        panel3 = panel4;
                        $("div.panel4 img.exit").unbind("click");
                        $("div.panel4").removeClass("panel4 " + panel4).addClass("panel3 " + panel3);
                        $("div.panel3 img.exit").click(function () { removePanel("panel3"); });
                        setPanelDimensions(3);
                        panel4 = "";
                        console.log("moved panel 4");
                    }
                }
            }
            break;
        case "panel2":
            moveStaticContent(panel, panel2);
            $("div.panel2 img.exit").unbind("click");
            $("div.panel2").remove();
            setPanelDimensions(1);
            panel2 = "";
            console.log("removed panel 2");

            if (panel3 != "") {
                panel2 = panel3;
                $("div.panel3 img.exit").unbind("click");
                $("div.panel3").removeClass("panel3 " + panel3).addClass("panel2 " + panel2);
                $("div.panel2 img.exit").click(function () { removePanel("panel2"); });
                setPanelDimensions(2);
                panel3 = "";
                console.log("moved panel 3");

                if (panel4 != "") {
                    panel3 = panel4;
                    $("div.panel4 img.exit").unbind("click");
                    $("div.panel4").removeClass("panel4 " + panel4).addClass("panel3 " + panel3);
                    $("div.panel3 img.exit").click(function () { removePanel("panel3"); });
                    setPanelDimensions(3);
                    panel4 = "";
                    console.log("moved panel 4");
                }
            }
            break;
        case "panel3":
            //if panel 4 is up then panel 3 = panel 4; remove panel 4
            moveStaticContent(panel, panel3);
            $("div.panel3 img.exit").unbind("click");
            $("div.panel3").remove();
            setPanelDimensions(2);
            panel3 = "";
            console.log("removed panel 3");
            
            if (panel4 != "") {
                panel3 = panel4;
                $("div.panel4 img.exit").unbind("click");
                $("div.panel4").removeClass("panel4 " + panel4).addClass("panel3 " + panel3);
                $("div.panel3 img.exit").click(function () { removePanel("panel3"); });
                setPanelDimensions(3);
                panel4 = "";
                console.log("moved panel 4");
            }
            break;
        case "panel4":
            moveStaticContent(panel, panel4);
            $("div.panel4 img.exit").unbind("click");
            $("div.panel4").remove();
            setPanelDimensions(3);
            panel4 = "";
            console.log("removed panel 4");
            break;
    }
    console.log("REMOVE PANEL -> end");
}

function moveStaticContent(panel, contentName) {
    $(".footerItem." + contentName).css({ "box-shadow": "" })
    if ($("div." + panel).find("div#yahooContentSmall").length > 0) {
        $("body").append($("#" + contentName + "ContentSmall"));
        $("#" + contentName + "ContentSmall").css({ "display": "none" });
    } else if ($("div." + panel).find("div#yahooCompose").length > 0) {
        $("body").append($("#yahooCompose"));
        $("#yahooCompose").css({ "display": "none" });
        $("#yahooCompose").find(".toAddress").val("");
        $("#yahooCompose").find(".ccAddress").val("");
        $("#yahooCompose").find(".bccAddress").val("");
        $("#yahooCompose").find(".subject").val("");
        $("#yahooCompose").find(".attachments").val("");
        $("#yahooCompose").find(".body").text("");
    } else {
        console.log("append " + contentName);
        $("body").append($("#" + contentName + "Content"));
        $("#" + contentName + "Content").css({ "display": "none" });
    }            
}

function setPanelDimensions(panelCount) {
    switch (panelCount) {
        case 1:
            $("div.panel1").width($("#bodycontent").width() - 6);
            $("div.panel1").height($("#bodycontent").height() - 6);
            $("div.panel1 div.header").width($("div.panel1").width());
            break;
        case 2:
            $("div.panel1").width(($("#bodycontent").width() * 0.5) - 6);            
            $("div.panel1").height($("#bodycontent").height() - 6);
            $("div.panel2").width(($("#bodycontent").width() * 0.5) - 6);
            $("div.panel2").height($("#bodycontent").height() - 6);
            $("div.panel1 div.header").width($("div.panel1").width());
            $("div.panel2 div.header").width($("div.panel2").width());
           
            break;
        case 3: 
            $("div.panel1").width(($("#bodycontent").width() * 0.5) - 6);
            $("div.panel1").height($("#bodycontent").height() * 0.6 - 6);
            $("div.panel2").width(($("#bodycontent").width() * 0.5) - 6);
            $("div.panel2").height($("#bodycontent").height() * 0.6 - 6);
            $("div.panel3").width($("#bodycontent").width() - 6);
            $("div.panel3").height($("#bodycontent").height() * 0.4 - 6);
            $("div.panel1 div.header").width($("div.panel1").width());
            $("div.panel2 div.header").width($("div.panel2").width());
            $("div.panel3 div.header").width($("div.panel3").width());
            break;
        case 4: 
            $("div.panel1").width(($("#bodycontent").width() * 0.5) - 6);
            $("div.panel1").height($("#bodycontent").height() * 0.6 - 6);
            $("div.panel2").width(($("#bodycontent").width() * 0.5) - 6);
            $("div.panel2").height($("#bodycontent").height() * 0.6 - 6);
            $("div.panel3").width($("#bodycontent").width() * 0.5 - 6);
            $("div.panel3").height($("#bodycontent").height() * 0.4 - 6);
            $("div.panel4").width($("#bodycontent").width() * 0.5 - 6);
            $("div.panel4").height($("#bodycontent").height() * 0.4 - 6);
            $("div.panel1 div.header").width($("div.panel1").width());
            $("div.panel2 div.header").width($("div.panel2").width());
            $("div.panel3 div.header").width($("div.panel3").width());
            $("div.panel4 div.header").width($("div.panel4").width());
            break;
    }
    for (var i = 1; i <= panelCount; i++) {
        $("div.panel" + i).find("img.contentImage").width($("div.panel" + i + " .post:first").width());
        $("div.panel" + i).find("iframe.contentVideo").width($("div.panel" + i + " .post:first").width());
    }
}





//REDEFINE
function feedItemDragStart(event, element) {
    //DEBUG
    console.log("FEED ITEM DRAG START -> begin")
    //console.log("EVENT:");
    //console.log(event);

    dragSource = $(event.target);

    $("#dragImage").remove("img");
    if (dragSource.hasClass("youtube")) { $("#dragImage").append("<img src=\"images/drag-youtube.png\" />"); }
    if (dragSource.hasClass("soundcloud")) { $("#dragImage").append("<img src=\"images/drag-soundcloud.png\" />"); }
    if (dragSource.hasClass("facebook")) { $("#dragImage").append("<img src=\"images/drag-facebook.png\" />"); }
    if (dragSource.hasClass("twitter")) { $("#dragImage").append("<img src=\"images/drag-twitter.png\" />"); }
    if (dragSource.hasClass("instagram")) { $("#dragImage").append("<img src=\"images/drag-instagram.png\" />"); }
    if (dragSource.hasClass("google")) { $("#dragImage").append("<img src=\"images/drag-google.png\" />"); }
    if (dragSource.hasClass("skype")) { $("#dragImage").append("<img src=\"images/drag-skype.png\" />"); }
    if (dragSource.hasClass("linkedin")) { $("#dragImage").append("<img src=\"images/drag-linkedin.png\" />"); }
    if (dragSource.hasClass("dropbox")) { $("#dragImage").append("<img src=\"images/drag-dropbox.png\" />"); }
    $("#dragImage").css("display", "");

    //DEBUG
    console.log("FEED ITEM DRAG START -> end")
}

function feedItemDragStop(event, element) {
     //DEBUG
     console.log("FEED ITEM DRAG STOP -> begin")		    		    
     //console.dir(element);
     //console.log(dragSource);


     dragTarget = $(event.target);
     console.log(dragTarget);

     //if (event.toElement != null) { dragTarget = $(event.toElement); } else { dragTarget = $(event.target); }
     //console.log(JSON.stringify(dragTarget));
    // console.log(event.toElement);
     //console.log($(event));
     //console.log(event);
     //console.log(dragTarget.attr("class"));
     //console.log(dragTarget.hasClass("facebook").length);



     if (dragTarget.closest("#bodycontent").length > 0) {
         bodyContentDrop(event);
     } else if (dragTarget.attr("id") == "bodycontent") {
         bodyContentDrop(event);
     }
     if (dragTarget.closest("#pepper-pig").length > 0) {
         pepperDrop(event);
     } else if (dragTarget.attr("id") == "pepper-pig") {
         pepperDrop(event);
     }

     //DEBUG
     console.log("FEED ITEM DRAG STOP -> end")
}

function bodyContentDrop(event, ui) {
    //DEBUG
    console.log("BODY CONTENT DROP -> begin");           
    //console.log(event);
    //console.log("TARGET:");
    //console.log(JSON.stringify(dragSource));
    //console.log(JSON.stringify(dragTarget));	
    //console.log(dragSource);
    //console.log(dragTarget);
    //console.log("DRAG SOURCE:");
    //console.log(dragSource);

    var sourceType = $(dragSource).attr("class").split(" ")[1];
    console.log("SOURCE TYPE: ", sourceType);
    console.log("DRAG SOURCE: ", dragSource);
    console.log("DRAG TARGET: ", dragTarget);

    console.log("DRAG SOURCE INPUT DATA: ", dragSource.find('input.data').val());
    if (dragTarget.hasClass("dropbox") && !dragSource.hasClass("dropbox-item")) {
        var fileDetails = $.parseJSON(dragSource.find('input.fileDetails').val());

        var newFilePath = $("#dropboxContent .current-path").val();
        if (newFilePath == "/") {
            newFilePath = fileDetails[0].name;
        } else {
            newFilePath += "/" + fileDetails[0].name;
        }     

        WriteDropbox(newFilePath, dragSource.find('input.data').val());
    }

    if (dragTarget.hasClass('facebook') && !dragSource.hasClass('facebook-post')) {
        if (dragSource.hasClass('dropbox-item')) {
            console.log("URL: ", dragSource.find('input.file-path').val());
            dropbox.makeUrl(dragSource.find('input.file-path').val(), { long: true }, function (error, response) {
                console.log("MAKE URL: ", response);
                WriteFacebookPost('Test image from dropbox from app 2', response.url, null);
            });
            
        }
    }

    if (dragTarget.hasClass('twitter') && !dragSource.hasClass('twitter-item')) {
        PostToTwitter();
        //if (dragTarget.closest(".panel1").length != 0) {
        //    dropPostItem(dragSource, "panel1", dragTarget);
        //} else if (dragTarget.closest(".panel2").length != 0) {
        //    dropPostItem(dragSource, "panel2", dragTarget);
        //} else if (dragTarget.closest(".panel3").length != 0) {
        //    dropPostItem(dragSource, "panel3", dragTarget);
        //} else if (dragTarget.closest(".panel4").length != 0) {
        //    dropPostItem(dragSource, "panel4", dragTarget);
        //}
    }

    //var sourceType = $(dragSource).attr("class").split(" ")[1];
    //console.log(sourceType);

    //if (dragSource.hasClass("footerItem") && !dragSource.hasClass("disabled")) {			    
    //    if (dragTarget.attr("id") == "bodycontent") {
    //        dropCreatePanel(sourceType);
    //    } else {                    
    //        if (dragTarget.parents().hasClass("leftPanel") || dragTarget.hasClass("leftPanel")) {
    //            dropUpdatePanel(dragSource.attr("class").split(" ")[1], panel1);
    //        } else if (dragTarget.parents().hasClass("rightPanel") || dragTarget.hasClass("rightPanel")) {
    //            dropUpdatePanel(dragSource.attr("class").split(" ")[1], panel2);
    //        } else if (dragTarget.parents().hasClass("bottomPanel") || dragTarget.hasClass("bottomPanel")) {
    //            dropUpdatePanel(dragSource.attr("class").split(" ")[1], panel3);
    //        } 
    //    }
    //} else {
    //    if (dragSource.hasClass("feedItem") || dragSource.hasClass("searchItem")) {
    //        if (dragTarget.parents().hasClass("yahoo") || dragTarget.hasClass("yahoo")) {
    //            dropEmailItem(dragSource);
    //        } else if (dragTarget.parents().hasClass("dropbox") || dragTarget.hasClass("dropbox")) {
    //            dropDropboxItem(dragSource);
    //        } else if (dragTarget.closest(".panel1").length != 0) {
    //            dropFeedItem(dragSource, "panel1", dragTarget);
    //        } else if (dragTarget.closest(".panel2").length != 0) {
    //            dropFeedItem(dragSource, "panel2", dragTarget);
    //        } else if (dragTarget.closest(".panel3").length != 0) {
    //            dropFeedItem(dragSource, "panel3", dragTarget);
    //        } else if (dragTarget.closest(".panel4").length != 0) {
    //            dropFeedItem(dragSource, "panel4", dragTarget);
    //        } else {			            
    //            $("#footer img").each(function () {
    //                if ($(this).hasClass(sourceType) && !$(this).hasClass("disabled")) {
    //                    clickCreatePanel(sourceType);
    //                    dropFeedItem(dragSource, "panel1", $("div.panel1"));
    //                }
    //            });
    //        }
    //    } else if (dragSource.hasClass("post") || dragSource.hasClass("dropbox") /* && dragTarget.hasClass("postbar")*/) {
    //        if (dragTarget.parents().hasClass("yahoo") || dragTarget.hasClass("yahoo")) {
    //            dropEmailItem(dragSource);
    //        } else if (dragTarget.parents().hasClass("dropbox") || dragTarget.hasClass("dropbox")) {
    //            dropDropboxItem(dragSource);
    //        } else if (dragTarget.closest(".panel1").length != 0) {
    //            dropPostItem(dragSource, "panel1", dragTarget);
    //        } else if (dragTarget.closest(".panel2").length != 0) {
    //            dropPostItem(dragSource, "panel2", dragTarget);
    //        } else if (dragTarget.closest(".panel3").length != 0) {
    //            dropPostItem(dragSource, "panel3", dragTarget);
    //        } else if (dragTarget.closest(".panel4").length != 0) {
    //            dropPostItem(dragSource, "panel4", dragTarget);
    //        }
    //    } 
    //}
    console.log("BODY CONTENT DROP -> end");
}

function postItemDragStart(event) {
    dragSource = $(event.target);
    var contentName = dragSource.parent().attr("id").substring(0, dragSource.parent().attr("id").indexOf("Content"));
    $("#dragImage").remove("img");
    $("#dragImage").append("<img src=\"images/drag-" + contentName + ".png\" />");		  
    $("#dragImage").css("display", "");
}

function dropPostItem(item, panel, target) {
    //DEBUG
    console.log("DROP POST ITEM -> begin");

    //var targetName = target.attr("id");
    //var feedItem = item.clone();
    //var contentType = feedItem.find("input.contentType").val();
    //var content = "";
    //var url = "";
    //var user = "";
    //var via = "";
    ////var pepperText = "";
    //url = feedItem.find("input.data").val();
    //via = feedItem.find("span.user").text();

    //content = " " + feedItem.find("span.spanContent").text();
    ////console.log("PANEL:");
    ////console.log(panel);

    //if (target.closest("#facebookContent").length > 0) {
    //    targetName = "facebook";
    //    pepperText.push("Posted!");
    //    var videoWidth = $("div." + panel + " .post:first").width();
    //    if (($("div.leftPanel").length > 0 && $("div.rightPanel").length == 0) || ($("div.leftPanel").length == 0 && $("div.rightPanel").length) > 0) {
    //        videoWidth = videoWidth / 2;
    //        //console.log("WIDTH:");
    //        //console.log(videoWidth);
    //    }
    //    if (url != "") {
    //        switch (contentType) {
    //            case "image": url = "<br /><img class=\"contentImage\" width=\"" + $("div." + panel + " .post:first").width() + "\" style=\"height: auto; \" src=\"" + url + "\" />"; break;
    //            case "video": url = "<br /><iframe class=\"contentVideo\" width=\"" + videoWidth + "\" style=\"min-height: 350px;\" src=\"" + url + "\" frameborder=\"0\" allowfullscreen></iframe>"; break;
    //        }
    //    }
    //    //console.log(url)
    //} else if (target.closest("#twitterContent").length > 0) {
    //    targetName = "twitter";
    //    pepperText.push("Tweeted!");
    //    if (url != "") {
    //        url = getShortenedURL(url);
    //        url = "<br /><a style=\"text-decoration: none; color: rgb(40,170,225); \" href=\"" + url + "\">" + url + "</a>";
    //    }
    //}
    //var blank = $("." + targetName + "PostBlank").clone();
    //blank.find("div.post span.via").before("via ").html(via);
    //blank.find("div.post span.spanContent").html(content);
    //blank.find("div.post span.spanContent").after(url);
    //blank.find("div.post input.contentType").val(contentType);
    //blank.find("div.post input.data").attr("value", feedItem.find("input.data").val());
    //blank.find("input.fileDetails").val(item.find("input.fileDetails").val());

    //$("#" + targetName + "Content div:eq(0)").after(blank.html());
    //$("#" + targetName + "Content div:eq(1)").effect("highlight", {}, 1000);
    //$("#" + targetName + "Content div:eq(1)").draggable({
    //    containment: "window",
    //    start: function (event, ui) { postItemDragStart(event); },
    //    helper: function () { return $("#dragImage").clone(); },
    //    cursorAt: { left: -10, top: -10 }
    //});
    //$("#" + targetName + "Content div:eq(1) div.detailsWrapper").css("width", "90%");
    ////$("#shoutBubbleText").text(pepperText);
    ////$("#shoutBubble").fadeIn("fast").delay(1500).fadeOut("fast");

    //DEBUG
    console.log("DROP POST ITEM -> end");
}


//DROPBOX FUNCTIONS
function GetDropBoxFiles(path) {
       
    if (dropbox != null && dropbox.isAuthenticated()) {
        console.log("authenticated.");

        //dropbox.getAccountInfo(function (error, accountInfo) {
        //    if (error) {
        //        return showError(error);  // Something went wrong.
        //    } else { console.log(accountInfo); }
        //});

        dropbox.readdir(path, { httpCache: true }, function (error, fileNames, folderStat, stats) {
            if (error) {
                return showError(error);  // Something went wrong.
            }

            var folders = new Array();
            var files = new Array();
            
            for (var i = 0; i < stats.length; i++) {
                if (stats[i].mimeType == 'inode/directory') {
                    folders.push(stats[i]);
                } else {
                    files.push(stats[i]);
                }
            }
            folders.reverse();
            files.reverse();

            console.log(files);

            var dropboxPreviousPath = null;
            if ($("#dropboxContent .previous-path").length == 0) {
                dropboxPreviousPath = $(document.createElement('input'));
                dropboxPreviousPath.addClass('previous-path');
                dropboxPreviousPath.attr('type', 'hidden');
                dropboxPreviousPath.val(path);
                $("#dropboxContent").append(dropboxPreviousPath);
            } else {                
                dropboxPreviousPath = $("#dropboxContent .previous-path");
                if (path.lastIndexOf('/') == 0) {
                    dropboxPreviousPath.val('/');
                } else {
                    dropboxPreviousPath.val(path.slice(0, path.lastIndexOf('/')));
                }
            }

            var dropboxCurrentPath = null;
            if ($("#dropboxContent .current-path").length == 0) {
                dropboxCurrentPath = $(document.createElement('input'));
                dropboxCurrentPath.addClass('current-path');
                dropboxCurrentPath.attr('type', 'hidden');
                dropboxCurrentPath.val(path);
                $("#dropboxContent").append(dropboxCurrentPath);
            } else {
                $("#dropboxContent .current-path").val(path);
            }

            for (var i = 0; i < files.length; i++) {
                switch (files[i].mimeType) {
                    case "text/plain":
                        thumbnailUrl = "images/dropbox-text.png";
                        break;
                    case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                        thumbnailUrl = "images/dropbox-spreadsheet.png";
                        break;
                    case "application/pdf":
                        thumbnailUrl = "images/dropbox-pdf.png";
                        break;
                    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                        thumbnailUrl = "images/dropbox-document.png";
                        break;
                    case "audio/mpeg":
                        thumbnailUrl = "images/dropbox-audio.png";
                        break;
                    case "video/x-msvideo":
                        thumbnailUrl = "images/dropbox-video.png";
                        break;
                    case "image/jpeg":
                    case "image/png":
                    case "image/gif":
                        thumbnailUrl = dropbox.thumbnailUrl(files[i].path, { size: 'medium' });
                        break;
                    case "application/zip":
                        thumbnailUrl = "images/dropbox-archive.png";
                        break;
                    case "application/x-msdos-program":
                        thumbnailUrl = "images/dropbox-application.png";
                        break;
                }
                ShowDropBoxFile(files[i], thumbnailUrl, '');
            }

            for (var i = 0; i < folders.length; i++) {
                ShowDropBoxDirectory(folders[i]);
            }

            if (path != '/') {
                ShowDropboxBack(dropboxPreviousPath.val());
            }
        });
    } else {

        dropbox = new Dropbox.Client({ key: 'bjfvcwo6gc3xiv7' }); //re2you offline
        //dropbox = new Dropbox.Client({ key: 'koh1ffn03dvh3xz' }); //re2you_dev online
        dropbox.authenticate({ interactive: true }, function (error, dropbox) {
            if (error) {
                alert(error);
            } else {                
                GetDropBoxFiles(path);
            }
        });
    }
}

function ShowDropBoxDirectory(metadata) {
    console.log("ShowDropBoxFolder");
    var dropboxDiv = $(document.createElement('div'));
    dropboxDiv.addClass('dropbox-folder');    

    var dropBoxContent = $(document.createElement('div'));
    dropBoxContent.addClass('content');

    var dropboxThumbnail = $(document.createElement('img'));
    dropboxThumbnail.addClass('thumbnail');
    dropboxThumbnail.attr('src', 'images/dropbox-directory.png');

    var dropboxCaption = $(document.createElement('span'));
    dropboxCaption.addClass('caption');
    dropboxCaption.text(metadata.name);

    var dropboxPath = $(document.createElement('input'));
    dropboxPath.addClass('path');
    dropboxPath.attr('type', 'hidden');
    dropboxPath.val(metadata.path);

    dropBoxContent.append(dropboxThumbnail).append(dropboxCaption).append(dropboxPath);
    dropboxDiv.append(dropBoxContent);
    dropboxDiv.click(function (e) {
        $("#dropboxContent .dropbox-item").remove();
        $("#dropboxContent .dropbox-folder").remove();
        $("#dropboxContent .dropbox-back").remove();
        GetDropBoxFiles($(this).find('input').val());
    });
    
    $("#dropboxContent div:eq(0)").after(dropboxDiv); 
}

function ShowDropBoxFile(metadata, thumbnailUrl, url) {
    console.log("ShowDropBoxFile");
    var dropboxDiv = $(document.createElement('div'));
    dropboxDiv.addClass('dropbox-item');
    dropboxDiv.draggable({
        containment: "window",
        start: function (event, ui) { postItemDragStart(event); },
        helper: function () { return $("#dragImage").clone(); },
        cursorAt: { left: -10, top: -10 }
    });

    var dropBoxContent = $(document.createElement('div'));
    dropBoxContent.addClass('content');

    var dropBoxLink = $(document.createElement('a'));
    dropBoxLink.attr('href', '');

    var dropboxThumbnail = $(document.createElement('img'));
    dropboxThumbnail.addClass('thumbnail');
    if (thumbnailUrl != null) {
        dropboxThumbnail.attr('src', thumbnailUrl);
    } else {
        //display other thumbnail
    }

    var dropboxCaption = $(document.createElement('span'));
    dropboxCaption.addClass('caption');        
    dropboxCaption.text(metadata.name.substr(0, metadata.name.lastIndexOf('.')));

    var dropboxFilePath = $(document.createElement('input'));
    dropboxFilePath.addClass('file-path');
    dropboxFilePath.attr('type', 'hidden');
    dropboxFilePath.val(metadata.path);

    dropboxDiv.click(function (e) {
        if (metadata.mimeType.indexOf('image/') == 0) {
            $('#overlay').css({ 'top': $(window).scrollTop(), 'height': $(window).height() }).fadeIn('fast', function () {
                var dropboxDiv = $(document.createElement('div'));
                dropboxDiv.addClass('dropbox-preview');

                var dropboxContent = $(document.createElement('div'));
                dropboxContent.addClass('content');

                var dropboxPreview = $(document.createElement('img'));
                dropboxPreview.addClass('preview');
                dropboxPreview.attr('src', dropbox.thumbnailUrl(metadata.path, { size: 'l' }));

                dropboxPreview.load(function () {
                    dropboxPreview.css('box-shadow', '0 0 10px 3px rgb(40,170,225)');
                    dropboxContent.width($(this).width());
                    dropboxContent.height($(this).height());
                });

                dropboxDiv.css({ "top": 200, "left": (($(window).width() / 2) - $("#rightcolumn").width()) });

                dropboxContent.append(dropboxPreview);
                dropboxDiv.append(dropboxContent);

                $("#dropboxContent div:eq(0)").after(dropboxDiv);

                $(window).click(function () {
                    if ($('#overlay').is(':visible')) { $('#dropboxContent .dropbox-preview').remove(); $('#overlay').fadeOut('fast'); }
                    $(window).unbind('click');
                });
            });
        } else {
            dropbox.makeUrl(metadata.path, { download: true }, function (error, data) {
                window.open(data.url, "_blank");
            });
        }
    });
 

    dropBoxContent.append(dropboxThumbnail).append(dropboxCaption).append(dropboxFilePath);
    dropboxDiv.append(dropBoxContent);

    $("#dropboxContent div:eq(0)").after(dropboxDiv);
}

function ShowDropboxBack(path) {
    console.log("ShowDropboxBack");
    var dropboxDiv = $(document.createElement('div'));
    dropboxDiv.addClass('dropbox-back');

    var dropBoxContent = $(document.createElement('div'));
    dropBoxContent.addClass('content');

    var dropboxThumbnail = $(document.createElement('img'));
    dropboxThumbnail.addClass('thumbnail');
    dropboxThumbnail.attr('src', 'images/dropbox-directory.png');

    var dropboxCaption = $(document.createElement('span'));
    dropboxCaption.addClass('caption');
    dropboxCaption.text('Back...');

    dropBoxContent.append(dropboxThumbnail).append(dropboxCaption);
    dropboxDiv.append(dropBoxContent);
    dropboxDiv.click(function (e) {
        console.log("back click");
        $("#dropboxContent .dropbox-item").remove();
        $("#dropboxContent .dropbox-folder").remove();
        $("#dropboxContent .dropbox-back").remove();
        GetDropBoxFiles(path);
    });

    $("#dropboxContent div:eq(0)").after(dropboxDiv);
}

function GetBase64FromImageUrl(URL) {
    var img = new Image();
    img.onload = function () {
        var canvas = document.createElement("canvas");
        canvas.width = this.width;
        canvas.height = this.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(this, 0, 0);

        var dataURL = canvas.toDataURL("image/png");

        dataURL = Base64ToArrayBuffer(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
        WriteDropbox('', dataURL);
    }
    img.src = URL;
}

function Base64ToArrayBuffer(string_base64) {
    var binary_string = window.atob(string_base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        var ascii = binary_string.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes.buffer;
}

function WriteDropbox(name, url) {
    console.log("WriteDropbox");
    var img = new Image();
    img.onload = function () {
        var canvas = document.createElement("canvas");
        canvas.width = this.width;
        canvas.height = this.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(this, 0, 0);

        var dataURL = canvas.toDataURL("image/png");
        dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        var binary_string = window.atob(dataURL);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            var ascii = binary_string.charCodeAt(i);
            bytes[i] = ascii;
        }

        dropbox.writeFile(name, bytes.buffer, function (error, fileStat) {
            if (error) {
                console.log(error);
            } else {
                console.log(fileStat);
            }
        });
    }
    img.src = url;    
}


//FACEBOOK FUNCTIONS
function GetFacebookPosts() {
    console.log("GetFacebookPosts");
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_UK/all.js', function () {
        FB.init({
            appId: '546818502119973',
            xfbml: true,
            version: 'v2.3'
        });
        facebook = FB;
        facebook.getLoginStatus(function (response) {
            if (response.status === 'connected') {
                $("#facebookContent .facebook-post").remove();
                $("#facebookContent .facebook-actions").remove();
                var accessToken = response.authResponse.accessToken;
                console.log("GET LOGIN STATUS: ", response);
                facebook.api("/me/feed", function (response) {
                    console.log("COUNT: ", response.data.length);
                    var posts = response.data.reverse();
                    $.each(posts, function (key, item) {
                        if (item.status_type !== undefined) {
                            switch (item.status_type) {
                                case "wall_post":
                                    facebook.api("/" + item.from.id + "/picture", function (response) {
                                        item.from.pic = response.data.url;
                                        console.log("WALL POST: ", item);
                                        DisplayWallPost(item);
                                    });                                                                       
                                    break;
                                case "mobile_status_update":
                                    facebook.api("/" + item.from.id + "/picture", function (response) {
                                        item.from.pic = response.data.url;
                                        console.log("STATUS POST: ", item);
                                        DisplayStatusPost(item);
                                    });
                                    break;
                                case "shared_story":
                                    switch (item.type) {
                                        case "video":
                                            facebook.api("/" + item.from.id + "/picture", function (response) {
                                                item.from.pic = response.data.url;
                                                console.log("STATUS POST VIDEO: ", item);
                                                DisplayWallPostVideo(item);
                                            });
                                            break;
                                        case "link":
                                            facebook.api("/" + item.from.id + "/picture", function (response) {
                                                item.from.pic = response.data.url;
                                                console.log("STATUS POST LINK: ", item);
                                                DisplayWallPostLink(item);
                                            });
                                            break;
                                        default:
                                            console.log("DEFAULT SHARED STOREY TYPE: ", item);
                                            break;
                                    }
                                    break;
                                default:
                                    console.log("UNKNOWN STATUS TYPE: ", item);
                                    break;
                            }                           
                        } else if (item.type !== undefined) {
                            switch (item.type) {
                                case "status":
                                    console.log("STATUS UPDATE: ", item);
                                    break;
                                default:
                                    console.log("UNKNOWN TYPE: ", item);
                                    break;
                            }                           
                        } else {
                            console.log("OTHER: ", item);
                        }                        
                    });
                });
            } else {
                facebook.login(function () { }, { scope: 'read_stream, user_photos, user_friends, user_likes, user_status, read_friendlists, publish_actions' })
            }
        });
    });   
}

function DisplayWallPost(item) {
    
    var facebookDiv = $(document.createElement('div'));
    facebookDiv.addClass('facebook-post');

    var facebookContent = $(document.createElement('div'));
    facebookContent.addClass('content');

    var facebookUser = $(document.createElement('div'));
    facebookUser.addClass('user');    
       
    var facebookProfilePic = $(document.createElement('img'));
    facebookProfilePic.addClass('profile-pic');
    facebookProfilePic.attr('src', item.from.pic);
    
    var time = new Date(item.created_time).getHours() + ':' + new Date(item.created_time).getMinutes();
    facebookUser.append(facebookProfilePic).append('<div><span><a href="#">' + item.from.name + '</a></span><span class="right-arrow"></span><span><a href="#">' + item.to.data[0].name + '</a></span>');
    facebookUser.append('<span class="timestamp">' + time + ' ' + $.datepicker.formatDate("M d, yy", new Date(item.created_time)) + '</span></div>');

    var facebookBody = $(document.createElement('div'));
    facebookBody.addClass('body');
    facebookBody.append('<span>' + item.message + '</span>');

    var facebookLikes = $(document.createElement('div'));
    facebookLikes.addClass('likes');
    if (item.likes !== undefined) {
        if (item.likes.data.length == 1) {
            facebookLikes.append('<span>' + item.likes.data.length + ' like</span>');
        } else {
            facebookLikes.append('<span>' + item.likes.data.length + ' likes</span>');
        }
    }

    var facebookActions = $(document.createElement('div'));
    facebookActions.addClass('facebook-actions');

    var facebookActionsLike = $(document.createElement('span'));
    facebookActionsLike.addClass('like');
    facebookActionsLike.text('Like');
    var facebookActionsComment = $(document.createElement('span'));
    facebookActionsComment.addClass('comment');
    facebookActionsComment.text('Comment');
    var facebookActionsShare = $(document.createElement('span'));
    facebookActionsShare.addClass('share');
    facebookActionsShare.text('Share');

    facebookActions.append(facebookActionsLike).append(facebookActionsComment).append(facebookActionsShare);
    
    facebookContent.append(facebookUser).append('<div style="clear: both;"></div>').append(facebookBody).append(facebookLikes);
    facebookDiv.append(facebookContent);

    $("#facebookContent div:eq(0)").after(facebookDiv);
    $("#facebookContent div:eq(1)").after(facebookActions);
}

function DisplayStatusPost(item) {

    var facebookDiv = $(document.createElement('div'));
    facebookDiv.addClass('facebook-post');

    var facebookContent = $(document.createElement('div'));
    facebookContent.addClass('content');

    var facebookUser = $(document.createElement('div'));
    facebookUser.addClass('user');

    var facebookProfilePic = $(document.createElement('img'));
    facebookProfilePic.addClass('profile-pic');
    facebookProfilePic.attr('src', item.from.pic);

    var time = new Date(item.created_time).getHours() + ':' + new Date(item.created_time).getMinutes();
    facebookUser.append(facebookProfilePic).append('<div><span><a href="#">' + item.from.name + '</a></span>');
    facebookUser.append('<span class="timestamp">' + time + ' ' + $.datepicker.formatDate("M d, yy", new Date(item.created_time)) + '</span></div>');

    var facebookBody = $(document.createElement('div'));
    facebookBody.addClass('body');
    facebookBody.append('<span>' + item.message + '</span>');

    var facebookLikes = $(document.createElement('div'));
    facebookLikes.addClass('likes');
    if (item.likes !== undefined) {
        if (item.likes.data.length == 1) {
            facebookLikes.append('<span>' + item.likes.data.length + ' like</span>');
        } else {
            facebookLikes.append('<span>' + item.likes.data.length + ' likes</span>');
        }
        facebookContent.append(facebookUser).append('<div style="clear: both;"></div>').append(facebookBody).append(facebookLikes);
    } else {
        facebookContent.append(facebookUser).append('<div style="clear: both;"></div>').append(facebookBody);
    }

    facebookDiv.append(facebookContent);

    var facebookActions = $(document.createElement('div'));
    facebookActions.addClass('facebook-actions');

    var facebookActionsLike = $(document.createElement('span'));
    facebookActionsLike.addClass('like');
    facebookActionsLike.text('Like');
    var facebookActionsComment = $(document.createElement('span'));
    facebookActionsComment.addClass('comment');
    facebookActionsComment.text('Comment');
    var facebookActionsShare = $(document.createElement('span'));
    facebookActionsShare.addClass('share');
    facebookActionsShare.text('Share');

    facebookActions.append(facebookActionsLike).append(facebookActionsComment).append(facebookActionsShare);
        
    $("#facebookContent div:eq(0)").after(facebookDiv);
    $("#facebookContent div:eq(1)").after(facebookActions);
}

function DisplayWallPostVideo(item) {
    var facebookDiv = $(document.createElement('div'));
    facebookDiv.addClass('facebook-post');

    var facebookContent = $(document.createElement('div'));
    facebookContent.addClass('content');

    var facebookUser = $(document.createElement('div'));
    facebookUser.addClass('user');

    var facebookProfilePic = $(document.createElement('img'));
    facebookProfilePic.addClass('profile-pic');
    facebookProfilePic.attr('src', item.from.pic);

    var time = new Date(item.created_time).getHours() + ':' + new Date(item.created_time).getMinutes();
    facebookUser.append(facebookProfilePic).append('<div><span><a href="#">' + item.from.name + '</a></span></span>');
    facebookUser.append('<span class="timestamp">' + time + ' ' + $.datepicker.formatDate("M d, yy", new Date(item.created_time)) + '</span></div>');

    var facebookBody = $(document.createElement('div'));
    facebookBody.addClass('body');
    facebookBody.append('<span style="padding-bottom: 10px; display: block;">' + item.message.replace(/(?:\r\n|\r|\n)/g, '<br />') + '</span>');
    facebookBody.append('<iframe class="contentVideo" style="min-height: 158px;" src="' + item.source.replace('?autoplay=1', '') + '" frameborder="0"></iframe>');

    var facebookLikes = $(document.createElement('div'));
    facebookLikes.addClass('likes');
    if (item.likes !== undefined) {
        if (item.likes.data.length == 1) {
            facebookLikes.append('<span>' + item.likes.data.length + ' like</span>');
        } else {
            facebookLikes.append('<span>' + item.likes.data.length + ' likes</span>');
        }
        facebookContent.append(facebookUser).append('<div style="clear: both;"></div>').append(facebookBody).append(facebookLikes);
    } else {
        facebookContent.append(facebookUser).append('<div style="clear: both;"></div>').append(facebookBody);
    }
    facebookDiv.append(facebookContent);

    var facebookActions = $(document.createElement('div'));
    facebookActions.addClass('facebook-actions');

    var facebookActionsLike = $(document.createElement('span'));
    facebookActionsLike.addClass('like');
    facebookActionsLike.text('Like');
    var facebookActionsComment = $(document.createElement('span'));
    facebookActionsComment.addClass('comment');
    facebookActionsComment.text('Comment');
    var facebookActionsShare = $(document.createElement('span'));
    facebookActionsShare.addClass('share');
    facebookActionsShare.text('Share');

    facebookActions.append(facebookActionsLike).append(facebookActionsComment).append(facebookActionsShare);

    
    

    $("#facebookContent div:eq(0)").after(facebookDiv);
    $("#facebookContent div:eq(1)").after(facebookActions);
}

function DisplayWallPostLink(item) {
    var facebookDiv = $(document.createElement('div'));
    facebookDiv.addClass('facebook-post');

    var facebookContent = $(document.createElement('div'));
    facebookContent.addClass('content');

    var facebookUser = $(document.createElement('div'));
    facebookUser.addClass('user');

    var facebookProfilePic = $(document.createElement('img'));
    facebookProfilePic.addClass('profile-pic');
    facebookProfilePic.attr('src', item.from.pic);

    var time = new Date(item.created_time).getHours() + ':' + new Date(item.created_time).getMinutes();
    facebookUser.append(facebookProfilePic).append('<div><span><a href="#">' + item.from.name + '</a></span></span>');
    facebookUser.append('<span class="timestamp">' + time + ' ' + $.datepicker.formatDate("M d, yy", new Date(item.created_time)) + '</span></div>');

    var facebookBody = $(document.createElement('div'));
    facebookBody.addClass('body');
    facebookBody.append('<span style="padding-bottom: 10px; display: block;">' + item.message.replace(/(?:\r\n|\r|\n)/g, '<br />') + '</span>');
    facebookBody.append('<img style="max-height: 250px;" src="' + item.link.replace('www.dropbox.com', 'dl.dropboxusercontent.com') + '" />');

    var facebookLikes = $(document.createElement('div'));
    facebookLikes.addClass('likes');
    if (item.likes !== undefined) {
        if (item.likes.data.length == 1) {
            facebookLikes.append('<span>' + item.likes.data.length + ' like</span>');
        } else {
            facebookLikes.append('<span>' + item.likes.data.length + ' likes</span>');
        }
        facebookContent.append(facebookUser).append('<div style="clear: both;"></div>').append(facebookBody).append(facebookLikes);
    } else {
        facebookContent.append(facebookUser).append('<div style="clear: both;"></div>').append(facebookBody);
    }
    facebookDiv.append(facebookContent);

    var facebookActions = $(document.createElement('div'));
    facebookActions.addClass('facebook-actions');

    var facebookActionsLike = $(document.createElement('span'));
    facebookActionsLike.addClass('like');
    facebookActionsLike.text('Like');
    var facebookActionsComment = $(document.createElement('span'));
    facebookActionsComment.addClass('comment');
    facebookActionsComment.text('Comment');
    var facebookActionsShare = $(document.createElement('span'));
    facebookActionsShare.addClass('share');
    facebookActionsShare.text('Share');

    facebookActions.append(facebookActionsLike).append(facebookActionsComment).append(facebookActionsShare);

    $("#facebookContent div:eq(0)").after(facebookDiv);
    $("#facebookContent div:eq(1)").after(facebookActions);
}

function WriteFacebookPost(message, link, image) {
    console.log("WriteFacebookPost");
    facebook.api('/me/feed', 'POST',
        {
            message: message,
            link: link,
            image: image
        },
        function (response) {
            console.log("NEW POST: ", response);
            if (response && !response.error) {
                GetFacebookPosts();
            }
        }
    );
}


// TWITTER
function PostToTwitter() {
    console.log("PostToTwitter");
    var targetName;
    var feedItem = dragSource.clone();
    var contentType = feedItem.find("input.contentType").val();
    var content = "";
    var url = "";
    var user = "";
    var via = "";
    var item = dragSource;
    //var pepperText = "";
    url = feedItem.find("input.data").val();
    via = feedItem.find("span.user").text();

    alert("need to create twitter account and app then redo twitter blank div and create functions for getting tweets");

    content = " " + feedItem.find("span.spanContent").text();
    //console.log("PANEL:");
    //console.log(panel);

    targetName = "twitter";
    //pepperText.push("Tweeted!");
    console.log("URL: ", url);
    if (url != "") {
        //url = getShortenedURL(url);
        url = "<br /><a style=\"text-decoration: none; color: rgb(40,170,225); \" href=\"" + url + "\">" + url + "</a>";
    }
    var blank = $("." + targetName + "PostBlank").clone();
    blank.find("div.post span.via").before("via ").html(via);
    blank.find("div.post span.spanContent").html(content);
    blank.find("div.post span.spanContent").after(url);
    blank.find("div.post input.contentType").val(contentType);
    blank.find("div.post input.data").attr("value", feedItem.find("input.data").val());
    blank.find("input.fileDetails").val(item.find("input.fileDetails").val());

    $("#" + targetName + "Content div:eq(0)").after(blank.html());
    $("#" + targetName + "Content div:eq(1)").effect("highlight", {}, 1000);
    $("#" + targetName + "Content div:eq(1)").draggable({
        containment: "window",
        start: function (event, ui) { postItemDragStart(event); },
        helper: function () { return $("#dragImage").clone(); },
        cursorAt: { left: -10, top: -10 }
    });
    $("#" + targetName + "Content div:eq(1) div.detailsWrapper").css("width", "90%");
    //$("#shoutBubbleText").text(pepperText);
    //$("#shoutBubble").fadeIn("fast").delay(1500).fadeOut("fast");
}

function dropDropboxItem(item) {
    console.log("DROP DROPBOX ITEM -> begin");
    if (item.find("input.contentType").val() == "image") {
        var blank = $("div.dropboxBlank").clone();
        var fileDetails = $.parseJSON(item.find("input.fileDetails").val());
        blank.find("span.spanContent").html(fileDetails[0].name);
        blank.find("div.imageWrapper img").attr("src", item.find("input.data").val());
        blank.find("input.contentType").val(item.find("input.contentType").val());
        blank.find("input.data").val(item.find("input.data").val());
        blank.find("input.fileDetails").val(item.find("input.fileDetails").val());

        $("#dropboxContent div:eq(0)").after(blank.html());
        $("#dropboxContent div:eq(1)").effect("highlight", {}, 1000);
        $("#dropboxContent div:eq(1)").draggable({
            containment: "window",
            start: function (event, ui) { postItemDragStart(event); },
            helper: function () { return $("#dragImage").clone(); },
            cursorAt: { left: -10, top: -10 }
        });
        $("#shoutBubbleText").text("Dropped!");
        $("#shoutBubble").fadeIn("fast").delay(1500).fadeOut("fast");
    }
    console.log("DROP DROPBOX ITEM -> end");
}

function previewFeedItem(feedItem) {
    var preview = $("#feedItemPreview");
    var contentType = feedItem.find("input.contentType").val();
    var url = feedItem.find("input.data").val();
    preview.find(".imageWrapper img").attr("src", feedItem.find(".imageWrapper img").attr("src"));
    preview.find("span.user").html(feedItem.find("span.user").html());    
    preview.find("input.contentType").val(contentType);
    preview.find("input.data").val(url);
    preview.find("input.fileDetails").val(feedItem.find("input.fileDetails").val());

    if (url != "") {
        switch (contentType) {
            case "image": url = "<img class=\"contentImage\" src=\"" + url + "\" />"; break;
            case "video": url = "<iframe class=\"contentVideo\" src=\"" + url + "\" frameborder=\"0\" allowfullscreen></iframe>"; break;
        }
    }
    preview.find("span.spanContent").html(feedItem.find("span.spanContent").html() + "<br />" + url);

    $("#feedItemPreviewWrapper").css({ "top": feedItem.position().top, "left": feedItem.position().left - $("#feedItemPreviewWrapper").width() });
    $("#feedItemPreviewWrapper").show();
    $("#feedItemPreviewWrapper").height($("#feedItemPreview").height());
    //clearTimeout(feedTimeout);
}